package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.util.Scanner;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Burton Cai 
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	try {
        propertyRepository.addProperty(new Property(1, "24 Boston Ave, Malvern East VIC 3145, Australia", 5, 300, 200));
        propertyRepository.addProperty(new Property(2, "11 Bettina St, Clayton VIC 3168, Australia", 2, 300, 200));
        propertyRepository.addProperty(new Property(3, "3 Wattle Ave, Glen Huntly VIC 3163, Australia", 3, 300, 200));
        propertyRepository.addProperty(new Property(4, "3 Hamilton St, Bentleigh VIC 3204, Australia", 5, 300, 200));
        propertyRepository.addProperty(new Property(5, "82 Spring Rd, Hampton East VIC 3188, Australia", 4, 300, 200));
        System.out.println("5 properties added successfully");
    	}
    	catch(Exception e) {
    		System.out.print("An error occured when adding the new properties. ");
    	}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	try {
			for (Property element:propertyRepository.getAllProperties()) {
				System.out.println(element.getId() + " " + element.getAddress() + ", "
				+ element.getNumberOfBedrooms() + "BR(s) "
				+ element.getSize() + "sqm " + "$" + element.getPrice() + ",000.00");
			}
		} catch (Exception e) {
			System.out.print("An error occured in retrieving property list.");
		}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	Scanner sc = new Scanner(System.in);
        System.out.println("Enter the ID of the property you want to search: ");
        try {
        	int id = sc.nextInt();
			Property targetProperty = propertyRepository.searchPropertyById(id);
			System.out.println(targetProperty.getId() + " " + targetProperty.getAddress() + ", "
					+ targetProperty.getNumberOfBedrooms() + "BR(s) "
					+ targetProperty.getSize() + "sqm " + "$" + targetProperty.getPrice());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
        	sc.close();
        }
        
    }
    
    public void run() {
    	System.out.println(this.name);
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
